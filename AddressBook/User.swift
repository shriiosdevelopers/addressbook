//
//  User.swift
//  AddressBook
//
//  Created by sriramiOSDev on 23/09/18.
//  Copyright © 2018 sriramiOSDev. All rights reserved.
//

import Foundation

struct User {
    var name:String
    var mobile:Int
    var emailId: String
    var skypeId:String
    
}


