//
//  ContactsEditingViewController.swift
//  AddressBook
//
//  Created by sriramiOSDev on 23/09/18.
//  Copyright © 2018 sriramiOSDev. All rights reserved.
//

import UIKit

class ContactsEditingViewController: UIViewController {
    
    
    //Mark:Outlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var skypeLabel: UITextField!
    
    //Mark: Properties
    
    var modelController:ModelController? = nil
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let user = modelController?.contactList {
        
            nameTextField.text = user.name
            mobileTextField.text = "\(user.mobile)"
            emailTextField.text = user.emailId
            skypeLabel.text = user.skypeId
        }
        
    }

    // passBack
    @IBAction func save(_ sender: UIButton) {
        
        if let modelController = modelController, let name = nameTextField.text, let moblie = mobileTextField.text, let email = emailTextField.text, let skype = skypeLabel.text, !name.isEmpty && !moblie.isEmpty && !skype.isEmpty {
        
        let oldUser = modelController.contactList
            
            modelController.contactList = User(name: name, mobile: oldUser.mobile, emailId: email, skypeId: skype)
            dismissViewController()
            
        }else {
        
            //alert
            
            let title = "Missing one of the fields"
            let message = "All name,mobile, email, & skype to be present to be able to save your editing"
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(cancelAction)
            present(alertController, animated: true, completion: nil)
            
        }
        
        
        
        
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        
        dismissViewController()
    }
    
    func dismissViewController() {
        dismiss(animated: true, completion: nil)
        
    }

}
