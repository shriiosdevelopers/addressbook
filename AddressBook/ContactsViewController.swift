//
//  ViewController.swift
//  AddressBook
//
//  Created by sriramiOSDev on 23/09/18.
//  Copyright © 2018 sriramiOSDev. All rights reserved.
//

import UIKit

class ContactsViewController: UIViewController {
    
    //Mark: Outlets
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var skypeLabel: UILabel!
    
    //Mark:Properties
    
    fileprivate let modelController = ModelController()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let user = modelController.contactList
        
        nameLabel.text = user.name
        mobileLabel.text = "\(user.mobile)"
        emailLabel.text = user.emailId
        skypeLabel.text = user.skypeId
        
        
    }
    
    //Mark:Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let contactEditingViewController = segue.destination as! ContactsEditingViewController
        
        contactEditingViewController.modelController = modelController
        
    }


}

